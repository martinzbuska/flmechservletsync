/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flemch;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONException;

/**
 *
 * @author Martin
 */
@WebServlet(name = "FlemchServlet", urlPatterns = {"/"}, loadOnStartup = 1)
public class FlemchServlet extends HttpServlet {

    // Tabulka pre pamätanie si pripojenych klientov a pracu s nimi.
    private final HashMap<String, Socket> clients = new HashMap<>();
    // Objekt implemetacie servera na pripajanie klientov.
    private SocketServerImpl server;

    // Inicializacia servletu a Socket servera pre ziskavanie desktopovych klientov.
    @Override
    public void init() throws ServletException {
        super.init();
        try {
            server = new SocketServerImpl(1234, clients);
            server.start();
        } catch (IOException ex) {
            System.out.println("Nepodarilo sa vytvorit server na porte 8080.");
        }
    }

    // Zastavenie servera a ukoncenie vlakna, v ktorom bezal.
    // Vycistenie tabulky klientov.
    @Override
    public void destroy() {
        // Zrusenie vlakna socket servera.
        if (server != null) {
            server.stopServer();
        }
        // Zavretie vsetkych spojeni s klientami a zmazanie obsahu hash tabulky.
        synchronized (clients) {
            for (Socket s : clients.values()) {
                try {
                    s.close();
                } catch (IOException ex) {
                    System.out.println("Nepodario sa zrusit klienta pri ruseni servera.");
                }
            }
            clients.clear();
        }
        super.destroy();
    }

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        // Ziskanie zoznamu pouzivatelov, pre ktorych je sprava urcena.
        //String usersJSN = request.getParameter("users");

        // Ziskanie obsahu spravy, ktora je v JSON-e
        String filesJSN = request.getParameter("files");


        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet FlemchServlet</title>");
            out.println("</head>");
            out.println("<body>");
            //System.out.println(usersJSN);
            synchronized (clients) {
                if (clients.isEmpty()) {
                    out.println("<h1>Nobody connected to server.</h1>");
                } else {

                    // Rozoslanie spravy vsetkym klientom.
                    out.println("<h1>Sending files for all users.</h1>");
                    DataOutputStream outStream;
                    for (Socket clientSocket : clients.values()) {
                        try {
                            outStream = new DataOutputStream(clientSocket.getOutputStream());
                            outStream.writeUTF(filesJSN);
                        } catch (IOException ex) {
                            System.out.println("Chyba pri zasielani spravy klientovi " + clientSocket.getInetAddress());
                            clientSocket.close();
                            System.out.println("Klient bol odstraneny zo zonamu = " + clients.values().remove(clientSocket));
                        }
                    }
                    out.println("<h1>Sending files completed.</h1>");


                    // Pripravene pre posielanie konkretnym pouzivatelom.
                        /*
                     try {
                     JSONArray jarray = new JSONArray(usersJSN);
                     String userName;
                     Socket clientSocket;
                     for (int i = 0; i < jarray.length(); i++) {
                     userName = (String) jarray.getJSONObject(i).getString("name");
                     clientSocket = clients.get(userName);

                     if (clientSocket == null) {
                     out.println("<h1>Servlet SocketServlet without socket of " + userName + "</h1>");
                     } else {
                     try {
                     DataOutputStream outStream = new DataOutputStream(clientSocket.getOutputStream());
                     outStream.writeUTF(filesJSN);
                     } catch (IOException ex){
                     System.out.println("chyba pri zapise do klienta s menom    "+userName);
                     clientSocket.close();
                     clients.remove(userName);
                     }
                     }

                     }
                         
                     } catch (JSONException ex) {
                     System.out.println("Nepodarilo sa vytvorit JSONArray mien pouzivatelov.");
                     }
                     */

                }
            }


            out.println("</body>");
            out.println("</html>");
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
