/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flemch;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;

/**
 *
 * @author Martin
 */
public class SocketServerImpl extends Thread {
    
    private final ServerSocket serverSocket;
    private final HashMap<String, Socket> clients;
    private boolean running;
    
    public SocketServerImpl(int port, HashMap<String, Socket> clients) throws IOException{
        serverSocket = new ServerSocket(port, 100, InetAddress.getLocalHost()); //new ServerSocket(port); 
        this.clients = clients;
        this.running = true;
    }
    
    
    @Override
    public void run(){
        while (running){
            System.out.println("Bezim **************************************************************************************************");
            try {
                // Pripojenie noveho klienta.
                System.out.println("Idem robit blokujucu operaciu -----------------------------------------------------------");
                Socket newClient = serverSocket.accept();
                
                DataInputStream in = new DataInputStream(newClient.getInputStream());
                String idClient = in.readUTF();
                System.out.println("Id noveho klienta je " + idClient + ".");
                synchronized(clients){
                    clients.put(idClient, newClient);                    
                }                
                System.out.println("Novy klient pridany do zoznamu pripojenych klientov.");
            } catch (IOException ex) {
                System.out.println("Neuspesne pripojenie noveho klienta.");
            }
            
            
            
        }
        
        try {
            this.serverSocket.close();
        } catch (IOException ex) {
            System.out.println("Nepodarilo sa zrusit socket server.");
        }
        
    }
    
    
    
    public void stopServer(){
        this.running = false;        
    }
    
}
